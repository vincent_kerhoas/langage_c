#include <stdio.h> 

//======================================================================
// Funtions Prototypes
void calc_function(int*);	

//======================================================================
int main ()
{
static  int a=0;	
	
  a=5;
  printf("avant calcul : a = %d \n",a);
  
  calc_function(&a);
  printf("après calcul : a = %d \n", a);
  
  return 0;
}
//======================================================================
void calc_function(int* param)
{	
	printf("avant calcul : param = %d \n",*param);
	*param = *param+2 ;
	printf("après calcul : param = %d \n",*param);
}	
//======================================================================
