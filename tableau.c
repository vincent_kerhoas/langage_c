#include <stdio.h> 

//======================================================================
// Funtions Prototypes
void calc_function(int*);	

//======================================================================
int main ()
{
  static int tab[5]={1,2,3,4,5};
  printf("tab = 0x%lx \n",(long)tab);
  printf("before calc_function : tab[0] = %d, tab[1] = %d, tab[2] = %d, tab[3] = %d, tab[4] = %d \n",tab[0],tab[1],tab[2],tab[3],tab[4]);
  calc_function(tab);
  printf("after  calc_function : tab[0] = %d, tab[1] = %d, tab[2] = %d, tab[3] = %d, tab[4] = %d \n",tab[0],tab[1],tab[2],tab[3],tab[4]);
  return 0;
}
//======================================================================
void calc_function(int *param)
{	
	int i = 0;
	
	for(i=0 ; i < 5 ; i++)
	{		
		param[i] = param[i]+1;	
		//*(param + i) = *(param + i)+1; // alternate syntax				
	}	
}	
//======================================================================
