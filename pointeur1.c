#include <stdio.h> 

//======================================================================
// Funtions Prototypes
void calc_function(int);	

//======================================================================
int main ()
{
  static int a=5;
  printf("before calc_function : a = %d \n",a);
  
  calc_function(a);
  printf("after  calc_function : a = %d \n", a);
  
  return 0;
}
//======================================================================
void calc_function(int param)
{	
	printf("before calc : param = %d \n",param);
	param = param+2 ;
	printf("after  calc : param = %d \n",param);
}	
//======================================================================
