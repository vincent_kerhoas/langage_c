#include <stdio.h> 
//======================================================================
// Type definitions  
struct complex_TypeStruct
{ 
	float i ; 
	float j;
};
//======================================================================
// Funtions Prototypes
void calc_function(struct complex_TypeStruct*);	

//======================================================================
int main ()
{
  static struct complex_TypeStruct c1={1.2, 2.5}; // alternate syntax
  printf("&c1 = 0x%lx \n",(long)&c1);
  printf("before calc_function : c1.i=%f c1.j=%f \n",c1.i,c1.j);
  calc_function(&c1);
  printf("after  calc_function : c1.i=%f c1.j=%f \n",c1.i,c1.j);
  return 0;
}
//======================================================================
void calc_function(struct complex_TypeStruct *param)
{	
	param -> i = param -> i + 1.0; // (*param).i = (*param).i + 1.0 ;
	param -> j = param -> j + 1.0;
}	
//======================================================================
