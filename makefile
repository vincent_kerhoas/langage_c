PROJECT_NAME = first_prog
CC=gcc
PROJECT_SRC = .
vpath %.c $(PROJECT_SRC)

########################################################################
SRCS = main.c
SRCS += 

########################################################################
INC_DIRS = .
INCLUDE = $(addprefix -I,$(INC_DIRS))
########################################################################

CFLAGS=-std=c99 -g -U__STRICT_ANSI__ \
       -W -Wall -pedantic -O0 \
       -D_REENTRANT 

LDFLAGS=

########################################################################

all: $(PROJECT_NAME)

$(PROJECT_NAME): $(SRCS)
	$(CC) $(INCLUDE) $(DEFS) $(CFLAGS) $(WFLAGS) $(LDFLAGS) $^ -o $@

%.o: %.c
	$(CC) -c -o $@ $(INCLUDE) $(DEFS) $(CFLAGS) $(LDFLAGS) $^

clean:
	rm -f *.o $(PROJECT_NAME)

########################################################################

