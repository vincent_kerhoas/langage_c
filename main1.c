#include <stdio.h>


/* On utilise dans ce fichier [main1.c]
une fonction située dans un autre fichier */
extern void affichage(void);	


int main ()
{
  printf ("hello world \n");
  affichage() ;  // appel de la fonction affichage	
	
  return 0;
}
