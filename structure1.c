#include <stdio.h> 
//======================================================================
// Type definitions  
typedef struct 
{
	float i ; 
	float j;
} complex_TypeDef;	
//======================================================================
// Funtions Prototypes
void calc_function(complex_TypeDef*);	

//======================================================================
int main ()
{
  static complex_TypeDef c1 = {1.2, 2.5};
  printf("&c1 = 0x%lx \n",(long)&c1);
  printf("before calc_function : c1.i=%f c1.j=%f \n",c1.i,c1.j);
  calc_function(&c1);
  printf("after  calc_function : c1.i=%f c1.j=%f \n",c1.i,c1.j);
  return 0;
}
//======================================================================
void calc_function(complex_TypeDef *param)
{	
	param -> i = param -> i + 1.0; // (*param).i = (*param).i + 1.0 ;
	param -> j = param -> j + 1.0;
}	
//======================================================================
